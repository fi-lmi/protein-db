import argparse
import os
import subprocess
from pathlib import Path

from torchdrug import models, transforms, data, layers
from rdkit import Chem
from torchdrug.layers import geometry
import torch
import os
import pandas as pd
from pathlib import Path


# define model
WEIGHTS_PATH = "./trained_models/mc_gearnet_edge.pth"
transform = transforms.ProteinView(view="residue")

# instantiate model
graph_construction_model = layers.GraphConstruction(
    node_layers=[geometry.AlphaCarbonNode()],
    edge_layers=[
        geometry.SpatialEdge(radius=10.0, min_distance=5),
        geometry.KNNEdge(k=10, min_distance=5),
        geometry.SequentialEdge(max_distance=2),
    ],
    edge_feature="gearnet",
)
gearnet_edge = models.GearNet(
    input_dim=21,
    hidden_dims=[512, 512, 512, 512, 512, 512],
    num_relation=7,
    edge_input_dim=59,
    num_angle_bin=8,
    batch_norm=True,
    concat_hidden=True,
    short_cut=True,
    readout="sum",
)


net = torch.load(WEIGHTS_PATH)

device = torch.device("cuda")
gearnet_edge.load_state_dict(net)
gearnet_edge.eval().to(device)


def compute_embeddings(model):
    """Compute embeddings for all proteins in data/pdb and store them in data/embeddings.csv"""
    embeddings = {}
    proteins = os.listdir(DATA_PATH + "pdb-sample/")
    for protein in tqdm(proteins, total=len(proteins)):
        protein_path = DATA_PATH + "pdb-sample/" + protein
        embeddings[protein] = compute_single_embedding(model, protein_path)

    df = pd.DataFrame(list(embeddings.items()), columns=["protein", "embedding"])
    df.to_pickle(Path("gearnet_embeddings_10kpdb.pkl"))
    return df

def _untar(tar_file, input_dir, scratch_dir):
    """Helper function to paralelize untaring"""
    tar_file_path = str(os.path.join(input_dir, tar_file))
    subprocess.run(
        ["tar", "-xf", tar_file_path, "-C", scratch_dir, "--wildcards", "*.cif.gz"]
    )


def extract(input_dir, scratch_dir, index):
    """Extracts CHUNK of proteins on persistent storage from tars and moves them to the zip folder"""
    scrach_dir_loc = str(os.path.join(scratch_dir))
    if not os.path.exists(scrach_dir_loc):
        os.mkdir(scrach_dir_loc)

    # first untar and move to zip folder
    with open(index, "r") as index_f:
        print(f"Taking from index file {index}")
        proteins = index_f.readlines()
        for tar_protein_file in proteins:
            tar_file = tar_protein_file.split(",")[0].strip()
            _untar(tar_file, input_dir, scrach_dir_loc)

    # then unzip proteins itself
    subprocess.run(["gzip", "-dfr", str(scrach_dir_loc)])

    return scrach_dir_loc

def compute_single_embedding(protein_path):
    """Compute embedding using GearNet network"""
    # convert to pdb
    structure = parser.get_structure(protein_path.split("/")[-1][:4], file)
    io.set_structure(structure)
    io.save(protein_path + ".pdb")

    # convert to graph
    mol = Chem.MolFromPDBFile(protein_path + ".pdb", sanitize=False)
    if mol is None:
        raise ValueError("RDKit cannot read PDB file `%s`" % protein_path)
    protein = data.Protein.from_molecule(mol, "position", "length", "symbol", None, False)
    with protein.residue():
        protein.residue_feature = protein.residue_feature.to_dense()

def run(input_path, output_path, batch_size, fold_number=0):
    """Run GraSR embedding on a directory of cif files

    Args:
        input_path (str): Path to directory containing cif files
        output_path (str): Path to save embeddings as .pkl
        fold_number (int, optional): Fold number of the model. Accepts values 0-4.
        batch_size (int, optional): Batch size

    Returns:
        None, saves embeddings as .pkl
    """
    model = load_model(f"./GraSR/saved_models/grasr_fold{fold_number}.pkl")
    embeddings = {}

    proteins = os.listdir(input_path)
    proteins = [file for file in proteins if file.endswith(".cif")]
    for protein in proteins:
        subprocess.run([
            "python3", "/scripts/3d-af-surfer/mmcif_to_pdb.py", "--ciffile", input_path + protein
        ])

    for protein_batch in chunks(proteins, batch_size):
        x, ld, am = get_raw_feature_tensor(
            [f"{input_path}{protein}" for protein in protein_batch]
        )
        embeddings_result = model((x, x, ld, ld, am, am), True).detach().numpy()

        for key, value in zip(protein_batch, embeddings_result):
            embeddings[key.split("-")[1]] = value

    df = pd.DataFrame(list(embeddings.items()), columns=["protein", "embedding"])
    df.to_pickle(Path(output_path))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", type=str, required=True)
    parser.add_argument("--scratch_dir", type=str, required=True)
    parser.add_argument("--output", type=str, required=True)
    parser.add_argument("--batch_size", type=int, required=False, default=64)
    parser.add_argument("--position", type=int, required=False, default=0)
    parser.add_argument("--cache", type=bool, required=False, default=False)
    parser.add_argument("--index", type=str, required=False, default=False)

    args = parser.parse_args()

    input_path = Path(args.input)
    output_path = Path(args.output)
    scratch_dir = Path(args.scratch_dir)
    assert input_path.exists()

    extracted_data = args.scratch_dir
    if not args.cache:
        extracted_data = extract(input_path, scratch_dir, args.index)

    run(
        extracted_data,
        os.path.join(output_path, f"cif2grasr-result-{args.position}"),
        args.batch_size,
    )
