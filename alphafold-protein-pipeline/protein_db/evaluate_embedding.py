import json
import os
import subprocess
from copy import copy
from time import time

import numpy as np
import pandas as pd
from scipy.stats import spearmanr

SCRIPT_PATH = "/protein-db/alphafold-protein-pipeline/USalign/USalign"


def get_protein_tm_score(path, protein_x, output):
    """Compute TM-score for one protein with all proteins in the folder.

    Args:
        path (str): path to folder with cif or pdb proteins
        protein_x (str): name of protein to compute distances from
        output (str): path to output json file

    Returns:
        dict: sorted dictionary of TM-scores from protein_x to all other proteins in the folder.
            Dictionary is also saved as json during processing.
    """
    t = time()
    tm_score_dict = {}
    proteins = os.listdir(path)
    proteins = [file for file in proteins if file.endswith(".cif")]

    for protein_y in proteins:
        result = subprocess.run(
            [SCRIPT_PATH, path + protein_x, path + protein_y], capture_output=True
        )
        result = result.stdout.decode("utf-8")
        tm_score = float(result.split("TM-score=")[1].split()[0])
        tm_score_dict[protein_y.split("-")[1]] = tm_score

    # save distances sorted from most to least similar
    with open(output, "w") as f:
        sorted_dict = {
            k: v
            for k, v in sorted(
                tm_score_dict.items(), key=lambda item: item[1], reverse=True
            )
        }
        json.dump(sorted_dict, f)

    t = time() - t
    print(f"Ground truth for {protein_x} was computed in {t:.1f}")

    return sorted_dict


def get_embedding_distance(file, protein_name, distance_type) -> dict:
    """
    Compute distance between one protein and all proteins in the folder.

    Args:
        file (str): path to pickle file
        protein_name (str): name of protein to compute distances from
        distance_type (function): any distance function from scipy.spatial.distance - e.g. `cosine`,
            `euclidean`. List of them can be found at
            https://docs.scipy.org/doc/scipy/reference/spatial.distance.html .

    Returns:
        dict: sorted dictionary of distances from protein_name to all other proteins in the file.
            Dictionary is also saved as json during processing.
    """
    t = time()
    unpickled_df = pd.read_pickle(file)
    protein_x = unpickled_df[unpickled_df["protein"] == protein_name].iloc[0]

    # iterate through data frame and compute distances
    distance_dict = {}
    for _, protein_y in unpickled_df.iterrows():
        try:
            dist = distance_type(
                protein_x["embedding"].flatten(), protein_y["embedding"].flatten()
            )
        except Exception:
            dist = 100
        distance_dict[protein_y["protein"]] = float(dist)

    # save distances sorted from most to least similar
    file = file.split(".")[0]
    with open(
        f"{file}_{protein_name}_{distance_type.__name__}_distance.json", "w"
    ) as f:
        sorted_dict = {
            k: v for k, v in sorted(distance_dict.items(), key=lambda item: item[1])
        }
        json.dump(sorted_dict, f)

    t = time() - t
    print(
        f"Distance of type {distance_type.__name__} for {protein_name} was computed in {t:.1f}"
    )

    return sorted_dict


def compute_spearman(file1, file2, protein_filter=None) -> float:
    """
    Compute spearman correlation between two json files.

    Args:
        file1 (str): path to first json file
        file2 (str): path to second json file
        protein_filter (list, optional): list of proteins, if not None, correlation will be computed
            only from provided list.

    Returns:
        float: spearman correlation
    """
    with open(file1) as file:
        json_x = json.load(file)
    with open(file2) as file:
        json_y = json.load(file)

    if protein_filter:
        for key in json_x:
            if key not in protein_filter:
                del json_x[key]
                del json_y[key]

    rank_x = copy(json_x)
    rank_y = copy(json_y)
    for i, key in enumerate(json_x):
        rank_x[key] = i
        rank_y[key] = i

    res = spearmanr(list(rank_x.values()), list(rank_y.values()))
    return res.statistic
