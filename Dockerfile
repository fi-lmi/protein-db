FROM jupyter/minimal-notebook:python-3.8 as base
USER root

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get update && apt-get install -y libxml2 g++ vim coreutils nano git libxrender1 wget fakeroot tzdata locales && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN ln -fs /usr/share/zoneinfo/Europe/Prague /etc/localtime && dpkg-reconfigure --frontend noninteractive tzdata && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen

RUN conda clean --yes -c nvidia cuda-nvcc -a && rm -rf ~/.cache

RUN pip install --upgrade pip
RUN pip install tensorflow[and-cuda] && pip install torch && rm -rf ~/.cache
RUN pip install -U nvidia-cudnn-cu11

COPY . /protein-db
WORKDIR /protein-db

RUN pip install -r requirements.txt 
RUN rm -rf ~/.cache

# (Embedding) Prepare DeepFRI model
RUN git clone https://github.com/flatironinstitute/DeepFRI.git alphafold-protein-pipeline/DeepFRI
RUN wget https://users.flatironinstitute.org/~renfrew/DeepFRI_data/trained_models.tar.gz
RUN tar xvzf trained_models.tar.gz -C alphafold-protein-pipeline

# Prepare GearNet model
RUN git clone https://github.com/DeepGraphLearning/GearNet.git alphafold-protein-pipeline/GearNet
RUN wget https://zenodo.org/record/7593637/files/mc_gearnet_edge.pth
RUN mv mc_gearnet_edge.pth alphafold-protein-pipeline/trained_models/mc_gearnet_edge.pth

# Prepare GraSR model
RUN git clone https://github.com/katarinagresova/GraSR.git alphafold-protein-pipeline/GraSR

# (Ground truth) Prepare USAlign code
RUN git clone https://github.com/pylelab/USalign.git alphafold-protein-pipeline/USalign
RUN g++ -static -O3 -ffast-math -lm -o alphafold-protein-pipeline/USalign/USalign alphafold-protein-pipeline/USalign/USalign.cpp

FROM scratch

COPY --chown=1000:1000 --from=base / /

USER user

WORKDIR /protein-db

ENV PYTORCH_VERSION 2.0.1
ENV TZ Europe/Prague

#ENV PATH /opt/conda/bin:/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin

CMD jupyter-lab --no-browser --ip "0.0.0.0" --port 8888 --NotebookApp.password=''
